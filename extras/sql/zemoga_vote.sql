-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-03-2018 a las 23:44:02
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `zemoga_vote`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_files`
--

DROP TABLE IF EXISTS `brw_files`;
CREATE TABLE IF NOT EXISTS `brw_files` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_code` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_images`
--

DROP TABLE IF EXISTS `brw_images`;
CREATE TABLE IF NOT EXISTS `brw_images` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_code` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `brw_images`
--

INSERT INTO `brw_images` (`id`, `name`, `record_id`, `model`, `description`, `category_code`, `created`, `modified`) VALUES
(1, 'kanye.png', 1, 'Personage', '', 'main', '2018-03-14 22:56:13', '2018-03-15 20:33:01'),
(2, 'Mark.png', 2, 'Personage', '', 'main', '2018-03-15 20:28:10', '2018-03-15 20:28:10'),
(3, 'cristina.png', 3, 'Personage', '', 'main', '2018-03-15 20:29:11', '2018-03-15 20:29:11'),
(4, 'malala.png', 4, 'Personage', '', 'main', '2018-03-15 20:30:41', '2018-03-15 20:30:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_users`
--

DROP TABLE IF EXISTS `brw_users`;
CREATE TABLE IF NOT EXISTS `brw_users` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `root` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `brw_users`
--

INSERT INTO `brw_users` (`id`, `email`, `password`, `root`, `last_login`, `created`, `modified`) VALUES
(1, 'zemoga@test.com', '799376808fda519ac421619727a3fd08c1a681b8', 0, '2018-03-15 20:26:10', '2018-03-14 22:33:17', '2018-03-14 22:33:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personages`
--

DROP TABLE IF EXISTS `personages`;
CREATE TABLE IF NOT EXISTS `personages` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `positive_votes` int(11) DEFAULT NULL,
  `negative_votes` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `personages`
--

INSERT INTO `personages` (`id`, `name`, `positive_votes`, `negative_votes`, `created`, `modified`) VALUES
(1, 'Kanye West', 2, 6, '2018-03-14 22:56:13', '2018-03-14 22:56:13'),
(2, 'Mack Zuckerberg', 7, 4, '2018-03-15 20:28:10', '2018-03-15 20:28:10'),
(3, 'Cristina Fernández', 2, 8, '2018-03-15 20:29:11', '2018-03-15 20:29:11'),
(4, 'Malala Yousafzai', 10, 3, '2018-03-15 20:30:41', '2018-03-15 20:30:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brw_files`
--
ALTER TABLE `brw_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `record_id` (`record_id`),
  ADD KEY `model` (`model`);

--
-- Indices de la tabla `brw_images`
--
ALTER TABLE `brw_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `record_id` (`record_id`),
  ADD KEY `model` (`model`);

--
-- Indices de la tabla `brw_users`
--
ALTER TABLE `brw_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`email`);

--
-- Indices de la tabla `personages`
--
ALTER TABLE `personages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brw_files`
--
ALTER TABLE `brw_files`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `brw_images`
--
ALTER TABLE `brw_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `brw_users`
--
ALTER TABLE `brw_users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `personages`
--
ALTER TABLE `personages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
