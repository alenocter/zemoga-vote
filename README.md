Hola, voy a dejar algunas referencias para que sea más fácil la revisión de esta prueba 

Lo subi a un servidor de prueba para que lo puedan ver andando

http://zemoga.plusglobal.com.ar/

Use el framework cakephp principalmente para guardar en base de datos los votos para cumplir con el requerimiento que al recargar no se pierdan los votos, y como extra también pueden ver un back-end donde podrían subir más personajes para votarlos, dejo los accesos:

http://zemoga.plusglobal.com.ar/admin
Usuario: zemoga@test.com
Contraseña: 123

para correrlo en local deben correr el Xampp 5.5 y levantar el sql que dejo el link

https://bitbucket.org/alenocter/zemoga-vote/src/15b941feda40e2bd67e1f0451a1d9b7b620ec390/extras/sql/zemoga_vote.sql?at=master&fileviewer=file-view-default


también dejo los links de los archivos más importantes 

### Layout 

https://bitbucket.org/alenocter/zemoga-vote/src/15b941feda40e2bd67e1f0451a1d9b7b620ec390/app/View/Layouts/default.ctp?at=master&fileviewer=file-view-default

### View 

https://bitbucket.org/alenocter/zemoga-vote/src/15b941feda40e2bd67e1f0451a1d9b7b620ec390/app/View/Personages/index.ctp?at=master&fileviewer=file-view-default

### CSS

https://bitbucket.org/alenocter/zemoga-vote/src/15b941feda40e2bd67e1f0451a1d9b7b620ec390/public_html/css/default.css?at=master&fileviewer=file-view-default

### JS

https://bitbucket.org/alenocter/zemoga-vote/src/15b941feda40e2bd67e1f0451a1d9b7b620ec390/public_html/js/default.js?at=master&fileviewer=file-view-default
