<?php
class PersonagesController extends AppController {

	public function index() {
		$this->set('personages', $this->Personage->getAll());
	}

	public function add_vote($type, $id) {
		$this->layout = 'ajax';
		
		$personage = $this->Personage->findById($id);
		if($type == "up") {
			$positive_votes = $personage['Personage']['positive_votes'];
			$this->Personage->save(array(
				'id' => $id, 
				'positive_votes' => ($positive_votes+1),
			));
		} else {
			$negative_votes = $personage['Personage']['negative_votes'];
			$this->Personage->save(array(
				'id' => $id, 
				'negative_votes' => ($negative_votes+1),
			));
		}
		return true;
	}
}