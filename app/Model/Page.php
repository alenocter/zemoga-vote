<?php

class Page extends AppModel {

	public $name = 'Page';
	public $displayField = 'title';
	
	public $brwConfig = array(
		'names' => array(
			'plural' => 'Páginas',
			'singular' => 'Página',
			'gender' => 2,
		),
		'files' => array(
			'base_arg' => array(
				'name_category' => 'Bases y condiciones de Argentina',
				'index' => true,
				'description' => false
			),
			'base_chi' => array(
				'name_category' => 'Bases y condiciones de Chile',
				'index' => true,
				'description' => false
			),
		),	
		'fields' => array(
			'names' => array(
				'title' => 'Título',
				'text_arg' => 'Texto Argentina',
				'text_chi' => 'Texto Chile',
				'dayend' => 'Fecha de finalizacion',
			),
			'hide' => array(
				'text_arg',
				'text_chi',
				'enabled',
			),
		),	
		'actions' => array(
			'add' => false,
			'delete' => false,
			'edit' => true,
		),		
	);

	public function get() {
		$page = $this->find('first', array(
			'contain' => array('BrwFile'),
		));
		return $page;
	}

}