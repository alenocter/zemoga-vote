<?php
class Personage extends AppModel {

	var $name = 'Personage';
	var $displayField = 'name';
	var $order = 'Personage.id desc';

	var $brwConfig = array(
    	'names' => array(
			'singular' => 'Persona',
			'plural' => 'Personas',
			'gender' => 1,
		),
		'images' => array(
			'main' => array(
				'name_category' => 'Imagen ',
				'sizes' => array('80x80','810_900',  '1024_1024'),
				'index' => true,
				'description' => false
			),
		),
		'fields' => array(
			'filter' => array(				
				'name', 
			),
			'names' => array(
				'name' => 'Nombre',
				'positive_votes' => 'Cant. votos positivos',
				'negative_votes' => 'Cant. votos negativos',
			),
		),	
		'paginate' => array(
			'fields' => array(
				'id', 
				'name', 
				'positive_votes',
				'negative_votes', 
			),
			'images' => array('main'),
		),
    );

    public function getAll() {
    	$personages = $this->find('all', array(
    		'contain' => 'BrwImage', 
    		'order' => 'Personage.id Asc',
    	));

    	foreach ($personages as $key => $personage) {
    		$total = $personage['Personage']['positive_votes'] + $personage['Personage']['negative_votes'];
       		$percentagePositive = floor(($personage['Personage']['positive_votes'] * 100) / $total);
   			$percentageNegative = 100 - $percentagePositive;

   			$personages[$key]['Personage']['percentagePositive'] = $percentagePositive;
   			$personages[$key]['Personage']['percentageNegative'] = $percentageNegative;
   			if($percentagePositive >= 50) {
   				$personages[$key]['Personage']['up'] = true;
   			}
    	}

    	return $personages;
    }
}