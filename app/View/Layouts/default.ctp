<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $title_for_layout; ?></title>
		<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array(
			'reseter',			
			'fancybox/jquery.fancybox',
			'base',
			'bootstrap.min',
			'default', //Siempre el default último
		));
		echo $this->Html->script(array(
			'jquery-1.10.1.min',
			'fancybox/jquery.fancybox',
			'fancybox/jquery.fancybox.pack',
			'bootstrap.min',
			'default',			
		));
		echo $scripts_for_layout;
		?>
		<script>var APP_BASE = '<?php echo Router::url('/')?>';</script>
	</head>
	<body>
		<?php echo $this->element('ie_warning'); ?>
		<div id="container">
		   <header>
		   		<div class="menu-contain">
					<div class="container">
						<div class="row">
							<div class="col-xs-4">
								<a class="company-name" href="#">Rule of Thumb.</a> 
							</div>
							<div class="col-xs-8 text-right">
								<nav class="menu">
									<ul>
								    	<li><a href="#">Past Trial</a></li>
								    	<li><a href="#">How it works</a></li>
								    	<li><a href="#">Log In/ Sign Up</a></li>
								    	<li><a class="magnifying-glass" href="#"></a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div class="container ">
					<div class="row">
						<div class="question col-sm-6">
							<div class="row info">
								<p class="opinion">What’s your opinion</p>
								<h1>Pope Francis?</h1>
								<p>He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)</p>
								<div class="more-info">
									<img src="img/wiki.png">
									<a class="more" href="#">More information</a>
								</div>
								<p class="verdict">What’s Your Verdict?</p>
							</div>
							<div class="row vote">
								<a class="up col-xs-6"><img src="img/up.png"></a>
								<a class="down col-xs-6"><img src="img/down.png"></a>
							</div>
						</div>
					</div>	
				</div>	
				<div class="container-fluid">
					<div class="row">
						<div class="closing col-xs-4 text-right">
							<p>Closin in</p>
						</div>
						<div class="days col-xs-8 text-left">
							<h2><span>22</span> day</h2>
						</div>
					</div>
				</div>
			</header>
			<div id="main" class="container clearfix">
		   		<?php
				echo $this->Session->flash();
				echo $content_for_layout;
				?>
			</div>
		    <footer>
		    	<div class="container submit-name">	
		    		<div class="row">	
						<div class="col-sm-9 text-left">
		    				<p>Is there anyone else you would want us to add?</p>
		    			</div>
		    			<div class="col-sm-3">
		    				<button>Submit a name</button>
		    			</div>			
		    		</div>			
				</div>
				<div class="container footer-up">
					<div class="row">	
						<div class="col-sm-8 text-left">
							<nav class="menu">
								<ul>
							    	<li><a href="#">Terms and Conditions</a></li>
							    	<li><a href="#">Privacy Policy</a></li>
							    	<li><a href="#">Contact Us</a></li>
								</ul>
							</nav>
						</div>
						<div class="col-sm-4 text-right">
							<ul class="folow">
						    	<li>Folow Us</li>
						    	<li><a href="#"><img src="img/facebook.png"></a></li>
						    	<li><a href="#"><img src="img/twitter.png"></a></li>
							</ul>
						</div>
					</div>		
				</div>			
		    </footer>
		</div>		
	</body>
</html>