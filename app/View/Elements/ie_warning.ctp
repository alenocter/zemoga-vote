<?php
$browser = substr(env('HTTP_USER_AGENT'), 25, 8);
if ($browser == 'MSIE 7.0' or $browser == 'MSIE 6.0' or $browser == 'MSIE 5.5') {
?>
	<div class="ie_warning">
		<div>
			<p>Su navegador es antiguo. Recomendamos descargar alguno de los siguientes:</p>
			<ul>
			    <li><a href="http://www.getfirefox.com">Mozilla Firefox</a></li>
			    <li><a href="http://www.google.com/chrome">Google Chrome</a></li>
			    <li><a href="http://www.microsoft.com/es-es/download/internet-explorer.aspx">Internet Explorer</a></li>
			    <li><a href="http://www.apple.com/safari/">Safari</a></li>
			    <li><a href="http://www.opera.com/">Opera</a></li>
			</ul>
		</div>
	</div>
<?php } ?>