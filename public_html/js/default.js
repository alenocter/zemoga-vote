document.addEventListener("DOMContentLoaded", function(event) {
	vote();
	voteagain();
});

function vote() {
   $(".vote-container .form-vote button").click(function(){
       key = this.dataset.key;
       id = this.dataset.id;

       vote = $(".vote-container input[name=vote-"+key+"]:checked").val();
       
       if(vote == "up") {
           obj.personage[key].positive_votes ++;
           $.get(APP_BASE + 'personages/add_vote/up/' + id);
       } else {
           obj.personage[key].negative_votes ++;
           $.get(APP_BASE + 'personages/add_vote/down/' + id);
       }
       positive_votes = parseInt(obj.personage[key].positive_votes);
       negative_votes = parseInt(obj.personage[key].negative_votes);
       total = positive_votes + negative_votes;
       percentagePositive = Math.trunc((positive_votes * 100) / total);
       percentageNegative = 100 - percentagePositive;


    
		$(".personage .key-"+key+" .up-barra")[0].style.width = percentagePositive+"%";
		$(".personage .key-"+key+" .down-barra")[0].style.width = percentageNegative+"%";

		$(".personage .key-"+key+" .percentage-up .number").text(percentagePositive);
		$(".personage .key-"+key+" .percentage-down .number").text(percentageNegative);

       if(percentagePositive >= 50) {
			$(".personage .key-"+key+" h2").removeClass();
			$(".personage .key-"+key+" h2").addClass('up');
		} else {
			$(".personage .key-"+key+" h2").removeClass();
			$(".personage .key-"+key+" h2").addClass('down');
		}
       $(".personage .key-"+key+" .vote-container form").hide();
       $(".personage .key-"+key+" .vote-container .vote-againe").show();
 	
       return false;
   });
}

function voteagain() {
	$(".vote-container .vote-againe button").click(function(){
		key = this.dataset.key;
		$(".personage .key-"+key+" .vote-container form").show();
       	$(".personage .key-"+key+" .vote-container .vote-againe").hide();
	});
}
